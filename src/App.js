import React, { Component } from 'react';
import './App.css';

function Card({ data, onSelect }) {
  
  const handleClick = () => onSelect(data.name)
  
  const className = data.selected ? 'selected' : ''

  return <div
    onClick={handleClick} 
    className={`ui-card ${className}`} >
    <div>
      <h2>{data.name}</h2> 
      <h4>{data.year}</h4>
    </div>
    {data.selected && <div className="check">
      &#128169;
    </div>}
  </div>
}


class App extends Component {

  state = {
    languages: [
      {
        name: 'ruby',
        year: 1995,
        selected: false
      },
      {
        name: 'js',
        year: 1995,
        selected: false
      },
      {
        name: 'php',
        year: 1995,
        selected: false
      },
      {
        name: 'java',
        year: 1995,
        selected: false
      },
      {
        name: 'scala',
        year: 2004,
        selected: false
      },
      {
        name: 'elixir',
        year: 2011,
        selected: false
      },
      {
        name: 'crystal',
        year: 2014,
        selected: false
      },
      {
        name: 'haskell',
        year: 1990,
        selected: false
      },
      {
        name: 'C#',
        year: 2000,
        selected: false
      },
      {
        name: 'Go',
        year: 2009,
        selected: false
      },
      {
        name: 'C++',
        year: 1985,
        selected: false
      },
      {
        name: 'python',
        year: 1990,
        selected: false
      }              
    ]
  }

  select = name => {
    // const toggleSelected = (prevState) => {
    //   return {
    //     languages: prevState.languages.map(lang => {
    //       if (lang.name === name) {
    //         return { ...lang, selected: !lang.selected }
    //       } else {
    //         return lang
    //       }
    //     })
    //   }
    // }

    const copiedLang = [...this.state.languages]
    copiedLang.forEach(lang => {
      if (lang.name === name) {
        lang.selected = !lang.selected
      } 
    })

    this.setState({
      languages: copiedLang
    })
  }

  renderCard = (props) => {
    const className = props.selected ? 'selected' : ''
    return <div
      onClick={() => this.select(props.name)} 
      className={`ui-card ${className}`} 
      key={props.name}>
      {props.name}
    </div>
  }

  destroy = () => {
    this.setState(prevState => (
      {
        languages: prevState.languages.filter(lang => lang.selected === false)
      }
    ))
  }

  render() {
    const { languages } = this.state
    const selectedLanguages = languages.filter(lang => lang.selected)
    return (
      <div className="App">
        <header >
          <div
            style={{opacity: languages.some(lang => lang.selected) ? '1' : '0' }}
            className="btn"
            onClick={this.destroy}>
            {selectedLanguages.length} &#128169;
          </div>
        </header>

        <section className="languages">
          {languages.map(lang => 
            <Card
              key={lang.name} 
              data={lang} 
              onSelect={this.select} 
            />
          )}
        </section>
      </div>
    );
  }
}

export default App;
